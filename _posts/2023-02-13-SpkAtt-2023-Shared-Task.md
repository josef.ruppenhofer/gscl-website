---
layout: post
title: Shared Task on Speaker Attribution in German News Articles and Parliamentary Debates
tags: [Shared_Task]
lang: en
---


We are happy to announce a new shared task on Speaker Attribution in German, as part of the GermEval Campaign, co-located with the Conference for Natural Language Processing ([KONVENS 2023](https://www.thi.de/konvens-2023/)) in Ingolstadt, Germany, in Sep 2023. The goal of this shared task is the identification of speakers in  political debates and in news articles, and the attribution of speech events to their respective speakers. Being able to identify this information automatically, i.e., identifying who says what to whom, is a necessary prerequisite for a deep semantic analysis of unstructured text. For more information about the shared task, including the task settings, datasets, evaluation metrics and link to the registration form, please visit the shared task website at [CodaLab](https://codalab.lisn.upsaclay.fr/competitions/10431). The SpkAtt-2023 shared task is partially supported by the German Society for Computational Linguistics and endorsed by two of its Special Interest Groups, CPSS and IGGSA.

**Important Dates**

* April 1, 2023 - Training and development data release             
* June 15, 2023 - Test data release (blind)                         
* July 1, 2023 - Submissions open                                   
* July 31, 2023 - Submissions close                                 
* August 14, 2023 - System descriptions due                         
* September 7, 2023 - Camera-ready system paper deadline            
* September 18-22, 2023 - Workshop at KONVENS 2023                  

**Organizing team**

* Ines Rehbein, Simone Ponzetto (U-Mannheim)

* Fynn Petersen-Frey, Chris Biemann (U-Hamburg)

* Josef Ruppenhofer, Annelen Brunner (IDS Mannheim)

**Contact**


 fynn.petersen-frey at uni-hamburg.de, rehbein at uni-mannheim.de


