---
layout: post
title: PhD Event auf der KONVENS 2023 in Ingolstadt
tags: [konvens2023]
lang: de
---

Wir möchten alle Doktoranden im Bereich der maschinellen Sprachverarbeitung (oder angrenzender Fächer) einladen, am PhD-Event der KONVENS 2023 an der Technischen Hochschule Ingolstadt teilzunehmen.

Die Konferenz selbst bietet die ideale Gelegenheit, mit Arbeiten zur Computerlinguistik in Wissenschaft und Industrie in Kontakt zu kommen. Das PhD-Event soll einen Raum für (unterhaltsame) Diskussionen rund um alle Aspekte des Lebens als Doktorand im NLP bieten und zum Kennenlernen Ihrer Community dienen.

Doktoranden haben die Möglichkeit, ihr Thema in 3-4-minütigen Lightning Talks kurz vorzustellen und dabei Feedback zu erhalten und sich mit ihren Kommilitonen auszutauschen.

Es gibt auch eine Session, die zeigt, wie man Kreativität anwenden kann, um mit Schwierigkeiten umzugehen und Unsicherheiten zu bewältigen.
Die Session besteht aus:

* Kunsttherapeutisches Aufwärmen: Ausdruck von Emotionen durch Farbe
* Haupttätigkeit: Comiczeichnen und Geschichtenerzählen
* Kunsttherapeutischer Abschluss: Transformation unangenehmer Erfahrungen

Wir freuen uns, Sie bei dieser unterhaltsamen und informativen Veranstaltung begrüßen zu dürfen!
Weitere Informationen hier: https://www.thi.de/konvens-2023/
