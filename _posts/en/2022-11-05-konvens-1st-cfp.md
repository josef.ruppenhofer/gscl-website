---
layout: post
title: Announcement KONVENS 2023 in Ingolstadt
tags: [conference]
feature-img: "assets/img/feature-img/konvens2023.jpg"
thumbnail: "assets/img/thumbnails/feature-img/konvens2023.jpg"
lang: en
---

# Announcement: KONVENS 2023 in Ingolstadt in September 2023

KONVENS 2023 will take place from September 18-22, 2023 at the Technische Hochschule Ingolstadt (Bavaria, Germany). Next to its technical program, KONVENS will feature a lively exchange between academic researchers and colleagues from industry, as well as workshops, tutorials, shared tasks, and networking events.
We particularly invite our student members and PhD members to join the event - consider submitting an abstract about a recent project or your PhD topic.
The paper and abstract submission deadline is May 19, 2023.

We are also looking for tutorial, workshop and shared task proposals - the deadline for proposals is November 30, 2022.

[Image Credit](https://commons.wikimedia.org/wiki/File:Ingolstadt_Altes_Rathaus_2012_02.jpg) [Image License](https://creativecommons.org/licenses/by-sa/3.0/deed.en)
