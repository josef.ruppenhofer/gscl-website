---
layout: post
title: Teaching for NLP Workshop @ KONVENS 2023
tags: [konvens2023]
lang: en
feature-img: "assets/img/events/teach4nlp2023.jpg"
thumbnail: "assets/img/events/teach4nlp2023_thumb.jpg"
---

On September 17, 2023, the Teaching for NLP Workshop took place as part of the satellite program of KONVENS 2023 in Ingolstadt. It was organized by Jannik Strötgen (FH Karlsruhe), Margot Mieskes (Hochschule Darmstadt), Christian Wartena (Hochschule Hannover), Stefan Grünewald (Bosch), and Annemarie Friedrich (University of Augsburg). The workshop brought together over twenty professors, lecturers, trainers, and teaching assistants of natural language processing and computational linguistics from universities, universities of applied science, and industry.

The workshop itself followed an example of open discussion-style sessions and ad-hoc presentations. Participants chose to discuss what should be part of a "Perfect NLP curriculum" (note the irony, please), the special challenges that come with teaching heterogenous or groups coming from special non-technical backgrounds, how to activate students, how to use visualizations effectively, and much more. The workshop also featured eight poster presentations and an informal workshop dinner.

 <div style="float:left; width: 280px; min-height: 500px; margin-left: 10px">
    <img src="{{ '/assets/img/events/teach4nlp2023_01.jpg' | relative_url }}">
 </div>
  <div style="float:left; width: 280px; min-height: 500px; margin-left: 10px">
    <img src="{{ '/assets/img/events/teach4nlp2023_02.jpg' | relative_url }}">
 </div>
  <div style="float:left; width: 280px; min-height: 500px; margin-left: 10px">
    <img src="{{ '/assets/img/events/teach4nlp2023_03.jpg' | relative_url }}">
 </div>
  <div style="float:left; width: 280px; min-height: 500px; margin-left: 10px">
    <img src="{{ '/assets/img/events/teach4nlp2023_04.jpg' | relative_url }}">
 </div>
 
 <div style="clear:both;"></div>
