---
layout: post
title: GSCL Talks
tags: [talks]
lang: en
---

The GSCL Talks are back from the summer break. We will start on September 16, 2021 at 4 p.m. with a talk by Prof. Dr. Alexander Koller (Saarland University) on the subject of "Compositional Semantic Parsing" (details [here](https://gscl.org/de/events/talks/september-2021-research-talk)).