---
layout: post
title: CPSS@Konvens 2022
permalink: /arbeitskreise/cpss/cpss-2022/
tags: [conferences]
lang: en
---


# 2nd Workshop on Computational Linguistics for Political Text Analysis

Please find its  website here: [https://old.gscl.org/en/arbeitskreise/cpss/cpss-2022](https://old.gscl.org/en/arbeitskreise/cpss/cpss-2022)

