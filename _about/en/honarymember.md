---
layout: page
title: Honorary Members
thumbnail: "assets/img/ehrenmitglieder.jpg"
img: "assets/img/ehrenmitglieder.jpg"
tags: [resources]
lang: en
---

The German Society for Computational Linguistics and Language Technology
(German: Gesellschaft für Sprachtechnologie und Computerlinguistik e.V., GSCL) was founded in 1975 as "LDV-Fittings e.V.", later renamed to "Gesellschaft für Linguistische Datenverarbeitung (GLDV) e. V." to foster linguistic data processing. Since September 2008, it has its current name.


## Prof. Dr. Burghard Rieger  †

On September 20, 2012 in Vienna, the German Society for Computational Linguistics and Language Technology decided to appoint Prof. Dr. Burghard Rieger as honorary member. As chairman of the former Association for Computational Linguistics (1989-1993) and as editor of the scientific journal LDV-Forum, he made a significant contribution to the development of computational linguistics in Germany. The honours ceremony took place on September 27, 2013 at the GSCL Conference in Darmstadt.

Professor Burghard Rieger passed away on July 19, 2021. With him, the GSCL loses a highly respected scientist and a longtime dedicated supporter of the association’s work. We will deeply miss him.

![]({{ "/assets/img/rieger.jpg" | relative_url }})

## Prof. Dr. Winfried Lenders †
On 01.10.2009 in Potsdam, the German Society for Computational Linguistics and Language Technology decided to appoint Prof. Dr. Winfried Lenders for its first honorary member. With the founding of the LDV-Fittings in 1975, Professor Lenders established the pathbreaking precursor of the GSCL. From 1993 to 1997, he chaired the then renamed Association for Computational Linguistics (GLDV), and was a long-standing member of the Advisory Board. His ongoing commitment contributed significantly to the profile of our scientific society.

Professor Winfried Lenders passed away on May 1, 2015. With him, the GSCL loses not only a highly respected scientist, but also a longtime dedicated supporter of the association's work. We will deeply miss him.

![]({{ "/assets/img/lenders-1408x.jpg" | relative_url }})
