---
layout: page
title: Förderung von Workshops
feature-img: "assets/img/events.jpg"
tags: [Workshop Funding]
lang: de
---

# Vergaberichtlinien für die Förderung von Workshops

Stand: 31. Januar 2023

## Allgemeines

Es gelten die folgenden Bedingungen für die Vergabe von Fördermitteln für GSCL-Workshops (über jeden Antrag und damit auch über Ausnahmen wird einzeln vom Vorstand der GSCL beschlossen):
Der Workshop wird von GSCL-Mitgliedern veranstaltet.
Die Möglichkeit zur Registrierung muss prinzipiell für alle GSCL-Mitglieder gegeben sein, optional darüber hinaus auch für weitere Teilnehmer:innen. Im Rahmen des Workshops sollen diese explizit dazu eingeladen werden, der GSCL beizutreten.
Der Workshop dient dem sprachtechnologischen bzw. computerlinguistischen Austausch. Auch inter- bzw. transdisziplinäre Inhalte sind willkommen.
 
## Antragstellung
Mindestens drei Monate vor der Durchführung des Workshops wird ein Antrag bei der Schatzmeisterin eingereicht, der folgende Informationen enthält:
1. Titel (Thema), Ort und Datum des Workshops.
2. Die voraussichtliche Anzahl aktiver Teilnehmer:innen (Beitragenden). Falls bereits eine vorläufige Teilnehmerliste besteht: eine Namensliste mit Angabe, ob eine GSCL-Mitgliedschaft besteht.
3. Die voraussichtliche Anzahl Teilnehmer:innen ohne eigenen Beitrag.
4. Eine möglichst namentliche Liste des/r geplanten Keynote Speaker(s).
5. Eine möglichst detaillierte Kostenplanung mit eingerechneter beantragter Fördersumme und mindestens den folgenden Angaben:
__Kosten__:<br/>
Kalkulation der benötigten Reisemittel (Reise, Unterkunft) pro aktiver/aktivem Teilnehmer:in / Keynote Speaker.
Voraussichtlich benötigte Mittel für die Verpflegung vor Ort (dabei ist kostensparend zu planen bzw. sind diese Kosten möglichst über Teilnahmegebühren zu decken).<br/>
__Einnahmen__:<br/>
Teilnahmegebühren (* Anzahl geplanter Teilnehmer:innen, siehe 3.) zur Deckung von Verpflegungs-, Raumkosten etc.
Geplante weitere Fördermittel unter Nennung der fördernden Institution(en).
Ob ein Vorschuss benötigt wird und wenn ja, in welcher Höhe.
 
## Durchführung
Eine Förderung ist an folgende Bedingungen gebunden:
Der Workshop wird für die wissenschaftliche Öffentlichkeit über die üblichen Medien wie die gängigen Verteiler, insbesondere aber über den GSCL-E-Mail-Verteiler bekannt gemacht (über Ausnahmen entscheidet der Vorstand).
Die Förderung durch die GSCL wird gut sichtbar dargestellt, auf der Webseite und allen weiteren schriftlichen Erzeugnissen, die sich auf den Workshop beziehen.
Der Workshop wird mit einer Webseite beworben, auf der die geplanten Beiträge sowie die Beitragenden erwähnt werden. Diese Webseite sollte nach Möglichkeit auf der Webseite der GSCL (http://www.gscl.org) angelegt oder mindestens von dort verlinkt werden. Dabei unterstützen selbstverständlich die Verantwortlichen der GSCL.
Mindestens die Beiträge der von der GSCL finanzierten aktiven Teilnehmer:innen / Keynote Speakers, möglichst jedoch alle Beiträge zum Workshop werden öffentlich zugänglich gemacht. Dies kann durch Präsenz-Teilnahmemöglichkeit, Live-Übertragungen und öffentlich zugängliche Aufzeichnungen bzw. kostenlos verfügbare Proceedings erfolgen.
 
## Abschließende Aktivitäten
Nach Durchführung des Workshops reichen die Antragsteller:innen verpflichtet, folgende Dokumente kurzfristig, spätestens zum 1.12. des Jahres, in dem er Workshop durchgeführt wird, ein:
Eine namentliche Liste der eingeladenen aktiven Teilnehmer:innen (Beitragenden) mit Angabe, ob bei diesen eine GSCL-Mitgliedschaft besteht. 
Einen Bericht für die GSCL-Webseite, der das Thema, den Ablauf und die aktiven Teilnehmer:innen (Vortragenden) sowie deren Beiträge beschreibt (100-200 Wörter).
(Fotos können, müssen aber nicht beigelegt werden.)
Eine Abrechnung der real angefallenen Kosten gegen die erzielten Einnahmen (Förderungen durch andere Institutionen, Vorschuss, erhaltene Teilnahmegebühren) im Vergleich zur abgegebenen Kalkulation, samt Belegen (Reise-/Unterkunftsbelege, Belege für Verpflegung sowie weitere beglichene Rechnungen). Falls sich ein Verlust bei dieser Abrechnung ergibt, kann mit der Abrechnung ein Nachantrag für den Differenzbetrag gestellt werden. Über die Übernahme dieser Kosten wird – abhängig von den noch verfügbaren Geldern aus dem Budget - jeweils im Einzelfall durch den Vorstand der GSCL entschieden. 
 


