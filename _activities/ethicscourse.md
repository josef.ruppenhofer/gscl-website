---
layout: page
title: Crash-Kurs Ethik in der Sprach- und Textverarbeitung
feature-img: "assets/img/sigs/ethics.jpg"
img: "assets/img/sigs/ethics.jpg"
feature-img: "assets/img/sigs/ethics.jpg"
permalink: /resources/ethics-crash-course
date: 27. Juni 2021
tags: [Ethik]
lang: de
---

<!-- Please ensure path https://gscl.org/resources/ethics-crash-course -- mentioned in paper! -->

Diese Seite beschreibt eine Reihe fertiger Unterrichtsmaterialien für einen Crash-Kurs in Ethik für die Verarbeitung natürlicher Sprache, die aus gemeinsamen GSCL-Arbeitssitzungen stammen. Die Materialien sind unter CC-BY veröffentlicht und können z.B. in NLP-Einführungskurse integriert werden. Wenn Sie die Materialien verwenden, zitieren Sie bitte:

> Annemarie Friedrich and Torsten Zesch. 2021. [A Crash Course on Ethics for Natural Language Processing](https://aclanthology.org/2021.teachingnlp-1.6) . In Proceedings of the Fifth Workshop on Teaching NLP, pages 49–51, Online. Association for Computational Linguistics.

Die Google-Folien finden Sie [hier](https://docs.google.com/presentation/d/1xt1FFtz67zWgIKOYy2puarORSthBb_rj3SfOuikvr-8/edit?usp=sharing). Wir empfehlen den direkten Zugriff auf die Google-Folien, um die jeweils aktuellste Version unserer Folien zu erhalten.
Falls Sie jedoch nicht auf Google-Folien zugreifen können, stellen wir auch [pptx][1] und [pdf][2] Versionen zum Download.

Wenn Sie Vorschläge haben, können Sie Ihre Kommentare gerne direkt zu den Google-Folien hinzufügen, aber senden Sie uns bitte auch eine [E-Mail](mailto:annemarie.friedrich@gmail.com).

[1]:{{ site.url }}/assets/crash_course_ethics_v2.pptx
[2]:{{ site.url }}/assets/crash_course_ethics_v2.pdf