---
layout: page
title: "GSCL Award for the best Student Thesis (BA/MA)"
lang: en
feature-img: "assets/img/award.jpg"
tags: [award]
---


Every two years, GSCL awards two prizes worth € 400 each for the best student undergraduate thesis and for the best master's thesis in the field of language technology and computational linguistics.

The nomination will be done by a supervisor. The lecturers at universities and technical colleges are called upon to encourage their best graduates of the past two years to submit a summary of their thesis. Based on a blind reviewing process, members of the GSCL Board select the best papers in a pre-selection process. These will then be presented by the authors at the KONVENS meeting in the final round. Travel costs and conference fees are borne by the GSCL.

The next selection round takes place in the summer of 2023. Candidates should have completed their work not earlier than April 2021. Theses from all German-speaking countries are acceptable (Austria, Germany and Switzerland) as well as from any other country, as long as the topic is focused on the German language.

The following documentation must be submitted in the PDF Format until 15th June 2023 to [gscl-preis@gscl.org](mailto:gscl-preis@gscl.org):

1. Summary of the work (German or English) with a maximum of five pages plus references and images. The authorship must not be evident from the abstract.
2. Name and email address of the author and supervisor, name of university/college, degree granted. The grade should not be given.
3. Brief statement / letter of recommendation from the supervisor or reviewer.


## Awardees since 2001

### 2021

-  Master: Marie Bexte (Duisburg): Combined Analysis of Image and Text Using Visio-Linguistic Neural Models – A Case Study on Robustness Within an Educational Scoring Task
-  Bachelor: Yannic Bracke (Potsdam): Automatic text classification with imbalanced data – Building a frame classifier from a corpus of editorials

### 2019

-  Master: Isabel Meraner (Zürich): Grasping the Nettle: Neural Entity Recognition for Scientific and Vernacular Plant Names
-  Bachelor: Rami Aly (Hamburg): Hierarchical writing genre classification with neural networks

### 2017

-  Master: Mathias Müller (Zürich): Treatment of Markup in Statistical Machine Translation
-  Bachelor: Katarina Ragna Krüger (Potsdam): Assessing the Dimensions of Factuality in Biomedical Text

### 2015

-  Master: Edo Collins (Tübingen): Classifying German Noun-Noun Compounds Using Stacked Denoising Autoencoders
-  Bachelor: Glorianna Jagfeld (Stuttgart): Towards a Better Semantic Role Labeling of Complex Predicates

### 2013

-  Marcel Bollmann (Bochum): Automatic Normalization for Linguistic Annotation of Historical Language Data
### 2011

Christian M. Meyer (Darmstadt): Combining Answers from Heterogenous Web Documents for Question Answering

### 2009

-  Christian Hardmeier (Basel): Using Linguistic Annotations in Statistical Machine Translation of Film Subtitles
-  Pierre Lison (Saarbrücken): Robust Processing of Spoken Dialogue

### 2007

Jette Klein-Berning (Heidelberg): Multilingual Information Retrieval with Latent Semantic Indexing

### 2005

Hans-Friedrich Witschel (Leipzig): Text, Words, Morphes: Possibilities of Automatic Terminology Extraction

### 2003

David Reitter (Potsdam): Rhetorical Analysis with Rich-Feature Support Vector Models

### 2001

Georg Rehm (Osnabrück): Preliminary Considerations for the Automatic Summary of German Texts Using an SGML and DSSSL-Based Representation of RST Relations
