---
layout: page
title: Corpus Linguistics
feature-img: "assets/img/sigs/books.jpg"
permalink:  /activities/corpuslinguistics/
tags: [SIG]
lang: de
---
Vorsitzende: Alexander Mehler, Armin Hoenen

Der Arbeitskreis Korpuslinguistik und quantitative Linguistik befasst sich mit der Entwicklung und Erprobung von Werkzeugen zur automatischen Analyse von Korpora sowie mit der Konstruktion und Anwendung mathematischer, quantitativer Modelle der explorativen Korpusanalyse. Der Arbeitskreis thematisiert folgende Fragestellungen:

* Aufbereitung und Annotation von Korpora.
* Korpusanalytisch basierte Metrisierung von Eigenschaften und Relationen sprachlicher Einheiten.
* Extraktion, Rekonstruktion bzw. Exploration sprachlichen Wissens aus Korpora natürlichsprachlicher Texte.
* Förderung von Anwendungen im Bereich der Textanalyse und Texttechnologie.
* Unterstützung der linguistischen Theorienbildung.
