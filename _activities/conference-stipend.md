---
layout: page
title: GSCL-Konferenzstipendium für Studierende (KONVENS 2023)
lang: de
feature-img: "assets/img/award.jpg"
tags: [award]
---

# GSCL Konferenzstipenden für BA-/MA-Studierende und Doktorand*innen für die Konvens 2023 in Ingolstadt

Die GSCL bietet Stipendien für Studierende und Doktorand*innen der Computerlinguistik und verwandten Fächern in Form der Übernahme der Teilnahmegebühren für den Besuch der KONVENS 2023 in Ingolstadt an. 

## Auswahlkriterien/Voraussetzungen
* Bewerber*innen müssen in einem Studiengang der Computerlinguistik oder einem verwandten Fach immatrikuliert (Doktoranden: mindestens registriert) sein. 
* Bewerber*innen müssen Mitglied der GSCL sein (der Antrag auf Mitgliedschaft sollte ggf. parallel zur Bewerbung für das Stipendium gestellt werden).
* Studierende, die exzellente Studienergebnisse vorweisen können und ein hohes Interesse an der Forschung zeigen, oder die bereits in GSCL aktiv sind, werden bevorzugt. 
* Ein eigener Beitrag für die Konferenz ist nicht erforderlich. Für Doktorand*innen ist die Teilnahme am PhD Event und/oder ein eigener Konferenzbeitrag erforderlich.

## Stipendienhöhe 
Das Stipendium übernimmt vordererst die Konferenzgebühr für die KONVENS 2023 in Ingolstadt (70 Euro). Eine Erstattung von Reise- und Unterbringungskosten kann ebenfalls beantragt werden, sie ist allerdings nur bei ausreichendem Gesamtbudget möglich. 
Bewerbungsmodalitäten
Bitten senden Sie Ihre Bewerbung, bis spätestens am 22.9.2023 an schatzmeister@gscl.org mit folgenden Informationen und Anlagen:
1. Name, Studiengang, Hochschule
2. Immatrikulationsnachweis
3. Tabellarischer Lebenslauf
4. Nachweis des Wohnorts (Ausweiskopie)
5. Beschreibung Ihrer Motivation: warum möchten Sie an der Konferenz teilnehmen, was erwarten Sie für sich davon (max. 300 Wörter) bzw. warum haben Sie an der Konferenz teilgenommen und was haben Sie für sich dort mitgenommen (halbseitiger Bericht)?

Die Entscheidung über den Antrag wird innerhalb von zwei Wochen nach Antragstellung erfolgen.
Im Falle eines Bedarfs an teilweiser Erstattung von Reise- und Unterkunftskosten bitten wir um eine Begründung und um Nachweis der angefallenen Kosten. Die Entscheidung über die entsprechende Erstattung fällt der Vorstand der GSCL, der Antrag wird selbstverständlich vertraulich behandelt. Die Erstattung erfolgt allerdings erst im Dezember 2023, nach Berücksichtigung aller studentischen Förderanträge.

DoktorandInnen verpflichten sich, (möglichst gemeinsam) einen Beitrag für die GSCL-Webseite über das PhD Event und weitere relevante Sessions auf der diesjährigen KONVENS zu erstellen. Selbstverständlich werden die AutorInnen des Artikels auf der Webseite genannt. Gerne dürfen auch Fotos für die Veröffentlichung auf der Webseite eingereicht werden. Der Artikel muss bis zum 15. Oktober 2023 an vorsitzende@gscl.org gesendet werden.
Das Stipendium kann im Lebenslauf als _GSCL Konferenzstipendium 2023_ genannt werden.
