---
permalink: /germeval
---


# GermEval Fragebogen

Der folgende Fragebogen hat zwei Hauptziele:

<ol>
<li>Der Fragebogen soll den Organisierenden der Task Hilfestellung geben, ethisch problematische Aspekte frühzeitig zu erkennen und so angemessen diskutieren oder beheben zu können.</li>
<li>Der Fragebogen dient dazu, strukturierte Informationen über die geplante Task zusammenzustellen, auf deren Grundlage die GSCL entscheidet, ob sie die Task als GermEval bewirbt.</li>
</ol>

## Task
<ul>
<li>Was ist die Fragestellung der Task?</li>
<li>Welche Sprache(n) werden adressiert?</li>
<li>Für welche Einsatzgebiete können die in der Shared Task entwickelten Systeme genutzt werden?</li>
<li>Wer profitiert wie vom Einsatz solcher Systeme?</li>
<li>Wie könnten solche Systeme missbraucht werden?</li>
<li>Wenn die Technologie funktioniert, wer könnte beeinträchtigt werden?</li>
<li>Wenn die Technologie nicht funktioniert, wer wird beeinträchtigt?.<br />
*Wenn die geplante Shared Task (oder bestimmte Teilaspekte der Task) unter ethischem     Gesichtspunkt als problematisch angesehen werden kann, was spricht dafür, sie trotzdem durchzuführen? Und wie können die mit der Task verbundenen Risiken minimiert werden?</li>
</ul>

## Daten
### Erzeugung
<ul>
<li>Wer hat die Daten für welchen Zweck erzeugt?</li>
<li>Aus welchen Quellen stammen die Daten und wie wurden sie gesammelt?</li>
</ul>

### Zusammenstellung
<p><em>Wie sind die Daten aufgebaut?<br />
</em>Wie viele Instanzen beinhaltet der Datensatz?<br />
<em>Sind die Daten repräsentativ für die Population, zu der geforscht wird?<br />
</em>Welche Bias könnten die Daten enthalten und welchen Einfluss haben diese auf die Ergebnisse?</p>
### Annotation
<p>*Wie wurden die Daten annotiert? (Was wird vom Annotationsschema erfasst? Wer hat die Daten annotiert? Angaben zum Annotationsprozess, Einwilligung der Annotatoren zur Veröffentlichung, etc.)</p>

### Verfügbarkeit
<p><em>Werden die Daten frei verfügbar sein?<br />
</em>Wie werden sie verteilt?<br />
*Welche Lizenz erhalten die Daten?</p>
<p>Darüber hinaus empfehlen wir Organisator*innen auch zusätzlich ein vollständiges Datasheet <a href="https://arxiv.org/abs/1803.09010">[Gebru et al., 2020]</a> zu veröffentlichen, um die Daten noch besser zu kontextualisieren.</p>

## Evaluation
<p>Die Organisierenden der Shared Task sollten sicherstellen, dass die Evaluation der Shared Task transparent, nachvollziehbar und fair ist, und dass die gewählten Evaluationsmaße bestmöglich das erfassen, was in der Beschreibung der Task als Untersuchungsgegenstand definiert wurde.<br />
Um Teams, deren Systeme nur niedrige bzw. negative Ergebnissen erzielten, zu motivieren, diese zu publizieren, könnten z.B. zusätzlich zu den besten Systemen auch Teams ausgezeichnet werden, die eine besonders gründliche und systematische Fehleranalyse präsentieren.<br />
Organisierende der Shared Task sollten nicht selbst an der Task teilnehmen, da sie über internes Wissen über die in der Task verwendeten Daten und mögliche Bias verfügen, das ihnen einen Vorteil im Wettbewerb verschaffen könnte. </p>
<ul>
<li>Sind die Evaluationskriterien zu Beginn der Task bekannt?</li>
<li>Werden die Daten und Evaluationsskripte nach Abschluss der Task der Forschungsgemeinde zugänglich gemacht, um die Replikation der Ergebnisse sowie weitere Forschung zum Thema zu ermöglichen?</li>
<li>Ist ein Closed-Track geplant, in dem alle Teams dieselben Ausgangsvoraussetzungen haben?</li>
<li>Ist ein Track geplant, der Analyse-Qualität über Leaderboard-Scores stellt?</li>
</ul> 
  
