---
layout: page
title: Infos
permalink: /infos/
feature-img: "assets/img/aboutus.jpg"
tags: [Page]
lang: de
---


## Informationen zur Computerlinguistik und zur Maschinellen Sprachverarbeitung

Die Computerlinguistik (CL) oder linguistische Datenverarbeitung (LDV) untersucht, wie natürliche Sprache in Form von Text- oder Sprachdaten mit Hilfe des Computers algorithmisch verarbeitet werden kann. Sie erarbeitet die theoretischen Grundlagen der Darstellung, Erkennung und Erzeugung gesprochener und geschriebener Sprache durch Maschinen und ist __Schnittstelle zwischen Sprachwissenschaft und Informatik__. In der englischsprachigen Literatur und Informatik ist neben dem Begriff Natural Language Processing (NLP) auch Computational Linguistics (CL) gebräuchlich. Der Fokus von NLP ist dabei eher auf der Erstellung von computerbasierten Methoden und Anwendungen für die Text- und Sprachverarbeitung, während sich die CL eher damit beschäftigt, mit Hilfe von computergestützten Methoden Erkenntnisse über Sprache zu erwerben. Mitglieder der GSCL repräsentieren das gesamte Spektrum dieser unterschiedlichen Ausprägungen der Maschinellen Sprachverarbeitung.
Ein guter Einstieg zu weiterführenden Informationen über die CL und NLP ist der Artikel über Computerlinguistik auf [Wikipedia](https://de.wikipedia.org/wiki/Computerlinguistik).

Wir versuchen eine möglichst vollständige alphabetische Liste von Studiengängen und Standorten bereitzustellen.
Falls Ihr Standort oder Studiengang nicht dabei ist, kontaktieren Sie [uns](mailto:informationsreferent@gscl.org) bitte.

## Computerlinguistik studieren
*Alphabetisch nach Standort*<br/>
*Seite im Aufbau!*

### Bachelor
* [Friedrich-Alexander-Universität Erlangen](https://www.linguistik.phil.fau.de/): Computerlinguistik (B.A.)
* [Universität Heidelberg](https://www.cl.uni-heidelberg.de/): Computerlinguistik (B.A.)
* [Heinrich-Heine-Universität Düsseldorf](https://www.ling.hhu.de/bereiche-des-institutes/abteilung-fuer-computerlinguistik): Computerlinguistik (B.A.)
* [Ludwig-Maximilians-Universität München](https://www.cis.uni-muenchen.de/): Computerlinguistik (B.Sc.)
* [Universität Potsdam](https://www.uni-potsdam.de/en/ling/index): Computerlinguistik (B.Sc.)
* [Universität des Saarlandes](https://www.lst.uni-saarland.de/): Language Science (B.A.), Computerlinguistik (B.Sc.)
* [Universität Stuttgart](https://www.ims.uni-stuttgart.de/): Maschinelle Sprachverarbeitung (B.Sc.)
* [Universität Trier](https://www.uni-trier.de/universitaet/fachbereiche-faecher/fachbereich-ii/faecher/computerlinguistik-und-digital-humanities/computerlinguistik): Sprache, Technologie, Medien (B.Sc.)
* [Eberhard-Karls-Universität Tübingen](https://uni-tuebingen.de/fakultaeten/philosophische-fakultaet/fachbereiche/neuphilologie/seminar-fuer-sprachwissenschaft/): Internationaler Studiengang Computerlinguistik (B.A.)
* [Universität Zürich](https://www.cl.uzh.ch/de.html): Computerlinguistik und Sprachtechnologie (B.A.) 

### Master
* [Universität Darmstadt](): Linguistic and Literary Computing (M.A.)
* [Universität Heidelberg](https://www.cl.uni-heidelberg.de/): Computerlinguistik (M.A.)
* [Ludwig-Maximilians-Universität München](https://www.cis.uni-muenchen.de/): Computerlinguistik mit Nebenfach (M.Sc.)
* [Universität des Saarlandes](https://www.lst.uni-saarland.de/): Language and Communication Technologies (M.Sc.), Language Science and Technology (M.Sc.)
* [Universität Stuttgart](https://www.ims.uni-stuttgart.de/): Computational Linguistics (M.Sc.)
* [Universität Trier](https://www.uni-trier.de/universitaet/fachbereiche-faecher/fachbereich-ii/faecher/computerlinguistik-und-digital-humanities/computerlinguistik): Natural Language Processing (M.Sc.)
* [Eberhard-Karls-Universität Tübingen](https://uni-tuebingen.de/fakultaeten/philosophische-fakultaet/fachbereiche/neuphilologie/seminar-fuer-sprachwissenschaft/): Internationaler Studiengang Computerlinguistik (M.A.)
* [Universität Zürich](https://www.cl.uzh.ch/de.html): Computational Linguistics & Language Technology (M.A.)

## Computerlinguistik- und NLP-Professuren
*Alphabetisch nach Nachname*<br/>
*Seite im Aufbau!*<br/>

### Deutschland

* Heike Adel-Vu, Hochschule der Medien Stuttgart
* Chris Biemann, Universität Hamburg
* Hendrik Buschmeier, Universität Bielefeld
* Christian Chiarcos, Universität Augsburg
* Vera Demberg, Universität des Saarlandes
* Lucie Flek, Universität Bonn
* Anette Frank, Universität Heidelberg
* Annemarie Friedrich, Universität Augsburg
* Alexander Fraser, Ludwig-Maximilians-Universität München
* Josef van Genabith, Universität des Saarlandes
* Munir Georges, Technische Hochschule Ingolstadt
* Goran Glavaš, Universität Würzburg
* Iryna Gurevych, Technische Universität Darmstadt
* Dietrich Klakow, Universität des Saarlandes
* Alexander Koller, Universität des Saarlandes
* Jonas Kuhn, Universität Stuttgart
* Katja Markert, Universität Heidelberg
* Jens Michaelis, Universität Bielefeld
* Margot Mieskes, Hochschule Darmstadt
* Günter Neumann, Universität des Saarlandes / DFKI
* Sebastian Padó, Universität Stuttgart
* Ulrike  Padó, Hochschule für Technik Stuttgart
* Barbara Plank, Ludwig-Maximilians-Universität München
* Simone Ponzetto, Universität Mannheim
* Nils Reiter, Universität Köln
* Stefan Riezler, Universität Heidelberg
* David Schlangen, Universität Potsdam
* Bernhard Schröder, Universität Duisburg-Essen
* Hinrich Schütze, Ludwig-Maximilians-Universität München
* Manfred Stede, Universität Potsdam
* Michael Strube, HITS & Universität Heidelberg
* Stefan Ultes, Universität Bamberg
* Christian Wartena, Hochschule Hannover
* Ngoc Thang Vu, Universität Stuttgart
* Andreas Witt, Universität Mannheim
* Sina Zarrieß, Universität Bielefeld
* Torsten Zesch, Fernuniversität Hagen
* Heike Zinsmeister, Universität Hamburg

_Junior-Professuren / Nachwuchsgruppen_
* Michael Hahn, Universität des Saarlandes
* Andrea Horbach, Fernuniversität Hagen
* Michael Roth, Emmy-Noether-Nachwuchsgruppe Universität Stuttgart
* Carina Silberer, Universität Stuttgart
* Gabriella Lapesa, Universität Stuttgart
* Tatjana Scheffler, Ruhr-Universität Bochum
* Anette Hautli-Janisz, Universität Passau


### Österreich

* Benjamin Roth, Universität Wien
* Michael Wiegand, Universität Klagenfurt


### Schweiz

* Cerstin Mahlow, Zürcher Hochschule für Angewandte Wissenschaften
* Rico Sennrich, Universität Zürich
* Martin Volk, Universität Zürich
* Mark Cieliebak, Zürcher Hochschule der Angewandten Wissenschaften

