# GSCL Website

This repository contains the source code for the website of the [**German Society for Computational Linguistics & Language Technology**][gscl].

[[_TOC_]]

## :construction_site: Structure

If you want to change content displayed on the website, arguably the most important folders to look at are the following:

* :file_folder: **`pages/`** contains the pages that can be reached from the main navigation (Events, Activities, About the GSCL) as well as a 404 pages, the imprint etc.
* :file_folder: **`_events/`** contains sub-pages of the “Events” page.
* :file_folder: **`_activities/`** contains sub-pages of the “Activities“ page (among them pages for special interest groups, publications and awards).
* :file_folder: **`_about/`** contains sub-pages of the “About the GSCL“ page.
* :file_folder: **`_posts/`** contains blog posts, e.g. announcements of Tutorial or Research Talks, Call for Papers, reports etc. Unlike pages and sub-pages, they are generally displayed in a long, chronologically ordered stream (in our case on the main page).
* :file_folder: **`_layouts/`** contains layout templates that are applied to the pages, sub-pages and blog posts. It also contains the template for the main page.

## :speaking_head: Multilingual Support

Can be used in conjunction with the multilingual [polyglot plugin](https://github.com/untra/polyglot).  
For those who don't need multilingual support / support for localizing other languages, ***the previous settings/default behavior should continue to apply***&mdash;at least to my knowledge.

### Features and Usage

- Multilingual features can be enabled by including `multilingual: true` in `_config.yml`  
If not set, the previous default behavior continues to apply
- If one doesn't have a multilingual site but wants to use a language other than English, one can set the language in `_config.yml` using `language: `, e.g. `language: de`  
That way, the `<html lang=...>` tag at the top of each page will reflect the language one is using.  
- Date formats can be localized/customized in `language.yml`, e.g. `date_format: "%d. %B, %Y"`.  
If one is using the polyglot plugin, one can place a `language.yml` file each into a separate directory for each language in `_data`, e.g. `_data/en/language.yml`, `_data/de/language.yml`---polyglot will automatically fetch the date_format (as well as all the other strings) according to the currently selected language.  
Otherwise, one can of course customize the date format (for a one-language site) in `_data/language.yml`   
- Capability to customize the  word order of localized strings strings with `"%s"`:  in languages other than English, this will sometimes be necessary, i.e. in German, "Auf Facebook folgen" is grammatically better than "Folgen auf Facebook"
This can now easily be implemented  by adding for example, ` str_follow_on: "Auf %s folgen"`  to the `language.yml` data file. The same can be done with `str_add_to`.
If no `"%s"` is specified in the localization string, the default behaviour of
appending it to the end will be applied.  
Also added `str_share` to `language.yml`---so that this can also be localized/translated if needed.  
- Language switcher in navbar: the language names need to be configured in _data/lang-names in order in implement this, e.g.:
```yaml
en: "English"
de: "Deutsch"
```
- Site title, description, header_text and footer_text are rendered according to the currently selected language (if `multilingual: true` is set). In order for this to work, one must create an entry for each language for each of the four site variables, e.g.:
```yaml
title:
  en: Site title
  de: Titel der Website
description:
  en: About this website
  de: Über diese Webseite
...
```

### Tutorials and How-To
* How to make smaller edits on posts: [Video](website_tutorials/AnleitungKleineAenderungen.mp4) (german)


## :scales: License and Sources

We use a modified version of the [Type-on-Strap][type-on-strap] theme, specially adapted to support multilingual use, particularly with the [Jekyll Polyglot plugin][polyglot].

### License for Type-on-Strap

This theme is licensed under the [The MIT License (MIT)](/LICENSE)

- Pictures from [Pexels](https://www.pexels.com/) are under Creative Commons Zero (CC0) license
- Fonts are licensed under the [SIL Open Font License (OFL)](https://scripts.sil.org/cms/scripts/page.php?site_id=nrsi&id=OFL) 

## :books: Further Reading

1. [Original README of Type-on-Strap](https://github.com/sylhare/Type-on-Strap/blob/master/README.md)

[gscl]:https://gscl.org
[type-on-strap]:https://github.com/sylhare/Type-on-Strap
[polyglot]:https://github.com/untra/polyglot
